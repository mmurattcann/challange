<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\IntegrationController;
use App\Http\Controllers\UserConroller;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::group(["middleware" => ["auth:api"]], function () {

    Route::post("/integration", [IntegrationController::class, "store"])->name("integration.store");
    Route::put("/integration/{id}", [IntegrationController::class, "update"])->name("integration.update");
    Route::delete("/integration/{id}", [IntegrationController::class, "destroy"])->name("integration.delete");;

});

Route::group(["middleware" => ["api.cors", "api.forceJson"]], function (){

    Route::post("/register", [UserConroller::class, "store"])->name("user.register");
    Route::post('/login', [UserConroller::class, "login"])->name('user.login');

});

