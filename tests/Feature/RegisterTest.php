<?php

namespace Tests\Feature;

use App\Models\User;
use GuzzleHttp\Client;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Facades\Hash;
use Laravel\Passport\Passport;
use Tests\TestCase;

class RegisterTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */

    public function test_cant_register_with_email()
    {
        $data = [
            "name" => "test",
            "password" => $this->faker->password
        ];

        $this->post(route("user.register"), $data)
            ->assertStatus(422);

    }

    public function test_cant_register_with_name(){
        $data = [
            "name" => null,
            "password" => $this->faker->password
        ];

        $this->post(route("user.register"), $data)
            ->assertStatus(422);
    }


    public function test_can_register(){
        $data = [
            "email" => "janedoe@test.com",
            "name" => "Jane DOE"
        ];

        $this->post(route("user.register"), $data)
            ->assertStatus(201);
    }

}
