<?php

namespace Tests\Feature;

use App\Models\Integration;
use App\Repositories\IntegrationRepository;
use Database\Factories\IntegrationFactory;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class IntegrationTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */

    protected $repository = null;

    public function __construct(?string $name = null, array $data = [], $dataName = '')
    {
        parent::__construct($name, $data, $dataName);
        $this->repository = new IntegrationRepository();
    }

    public function test_can_create_integration(){

        $data = [
            "marketplace" => $this->faker->company,
            "username" => $this->faker->userName,
            "password" => $this->faker->password
        ];

        $this->post(route("integration.store"), $data)->assertStatus(201)->assertJsonFragment($data);
    }


    public function test_can_update_integration(){

        $integration = $this->repository->model::factory()->create();


        $data = [
            "marketplace" => "Test",
            "username" => "MURAT",
            "password" => "123456"
        ];

        $this->put(route("integration.update", [ "id" =>$integration->id ]), $data)->assertStatus(201);
    }

    public function test_can_delete_integration(){
        $integration = $this->repository->model::factory()->create();
        $this->delete(route("integration.delete", ["id" => $integration->id]))->assertStatus(201);
    }
}
