<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class MarketPlaceSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    private $table = "market_places";

    public function run()
    {

        DB::table($this->table)->truncate();

        $data = [
            [
                "name" => "trendyol",
                "rank" => 1,
                "is_active" => 1
            ],
            [
                "name" => "n11",
                "rank" => 2,
                "is_active" => 1
            ],
            [
                "name" => "Hepsiburada",
                "rank" => 3,
                "is_active" => 1
            ],

        ];

        DB::table($this->table)->insert($data);
    }
}
