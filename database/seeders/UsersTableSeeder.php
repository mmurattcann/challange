<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table("users")->truncate();

        $data = [
            "name" => "John DOE",
            "email" => "johndoe@test.com",
            "password" => "123"
        ];

        DB::table("users")->insert($data);
    }
}
