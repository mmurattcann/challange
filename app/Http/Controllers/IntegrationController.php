<?php

namespace App\Http\Controllers;

use App\Http\Requests\IntegrationRequest;
use App\Services\IntegrationService;
use Illuminate\Http\Request;

class IntegrationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    protected  $integrationService = null;

    public function __construct()
    {
        $this->integrationService = new IntegrationService();
    }

    public function index()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  IntegrationRequest $request
     *
     */
    public function store(IntegrationRequest $request)
    {
        $response = ["status" => 201];

        try {
            $response["data"] = $this->integrationService->storeData($request);
        } catch (\Exception $e){
            $response = [
                "status" => 500,
                "error" => $e->getMessage(),
                "errorCode" => $e->getCode(),
            ];
        }

        return response()->json($response, $response["status"]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     *
     */
    public function update(IntegrationRequest $request, $id)
    {
        $response = ["status" => 201];

        try {
            $response["data"] = $this->integrationService->updateData($request, $id);
        } catch (\Exception $e){
            $response = [
                "status" => 500,
                "error" => $e->getMessage(),
                "errorCode" => $e->getCode(),
            ];
        }

        return response()->json($response, $response["status"]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     */
    public function destroy($id)
    {
        $response = ["status" => 201];

        try {
            $response["data"] = $this->integrationService->destroyData( $id);
        } catch (\Exception $e){
            $response = [
                "status" => 500,
                "error" => $e->getMessage(),
                "errorCode" => $e->getCode(),
            ];
        }

        return response()->json($response, $response["status"]);
    }
}
