<?php

namespace App\Http\Controllers;

use App\Http\Requests\LoginRequest;
use App\Http\Requests\UserRequest;
use App\Models\User;
use App\Services\UserService;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class UserConroller extends Controller
{
    protected $userService = null;

   public function __construct()
   {
        $this->userService = new UserService();
   }


    public function store(UserRequest $request)
    {


        $response = ["status" => 201];

        try {
            $response["data"] = $this->userService->storeData($request);
        } catch (\Exception $e){
            $response = [
                "status" => 500,
                "error" => $e->getMessage(),
                "errorCode" => $e->getCode(),
            ];
        }

        return response()->json($response, $response["status"]);
    }

    public function login (LoginRequest $request) {

        $user = User::where('email', $request->email)->first();

        if ($user) {
            if (Hash::check($request->password, $user->password)) {
                $token = $user->createToken('Laravel Password Grant Client')->accessToken;
                $response = ['token' => $token];
                return response($response, 200);
            } else {
                $response = ["message" => "Şifre Yanlış"];
                return response($response, 422);
            }
        } else {
            $response = ["message" =>'Bu e-posta ile kayıtlı herhangi bir kullanıcı bulunamadı.'];
            return response($response, 422);
        }
    }

}
