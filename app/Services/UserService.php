<?php


namespace App\Services;


use App\Http\Requests;
use App\Repositories\UserRepository;
use Illuminate\Http\Request;

class UserService
{
    public $userRepository = null;

    public function __construct()
    {
        $this->userRepository = new UserRepository();
    }

    public function storeData(Requests\UserRequest  $request){
        return $this->userRepository->store($request);
    }
}
