<?php


namespace App\Services;


use App\Http\Requests\IntegrationRequest;
use App\Repositories\IntegrationRepository;

class IntegrationService
{
    public $integrationRepository = null;

    public function __construct()
    {
        $this->integrationRepository = new IntegrationRepository();
    }

    public function storeData(IntegrationRequest  $request){
        return $this->integrationRepository->store($request);
    }

    public function updateData(IntegrationRequest $request, $id){
        return $this->integrationRepository->update($request, $id);
    }

    public function destroyData(int $id){
        return $this->integrationRepository->destroy($id);
    }

}
