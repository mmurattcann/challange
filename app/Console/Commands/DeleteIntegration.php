<?php

namespace App\Console\Commands;

use App\Repositories\IntegrationRepository;
use Illuminate\Console\Command;

class DeleteIntegration extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */

    private $repository = null;

    protected $signature = 'integration:delete {--id=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'This command generated to delete integration by given id number.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->repository = new IntegrationRepository();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $id = $this->option("id");

        if(! $id){
            $this->warn("Lütfen entegrasyonun ID'sini girin.");
            exit;
        }

        $this->repository->destroy($id);
        $this->info($id. " ID Numaralı Entegrasyon Silindi.");
    }
}
