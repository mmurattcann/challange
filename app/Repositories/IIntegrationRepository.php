<?php


namespace App\Repositories;


use App\Http\Requests\IntegrationRequest;
use App\Models\Integration;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use phpDocumentor\Reflection\Types\Boolean;

interface IIntegrationRepository
{
     public function baseQuery(): Builder;

    public function getById(int $id): Integration;

    public function store(IntegrationRequest  $request): Integration;

    public function update(Request $request, int $id): int;

    public function destroy(int $id);

}
