<?php


namespace App\Repositories;


use App\Http\Requests\IntegrationRequest;
use App\Models\Integration;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;

class IntegrationRepository implements IIntegrationRepository
{
    public $model = null;
    protected $relations = null;

    public function __construct()
    {
        $this->model = new Integration();
        $this->relations = [];
    }


    public function baseQuery(): Builder
    {
        return $this->model::query()->with($this->relations);
    }

    public function getById(int $id): Integration
    {
        return $this->baseQuery()->find( $id);
    }

    public function store(IntegrationRequest $request): Integration
    {
        return $this->baseQuery()->create($request->only($this->model->getFillable()));
    }

    public function update(Request $request, int $id): int
    {
        return $this->getById($id)->update($request->only($this->model->getFillable()));
    }

    public function destroy(int $id)
    {
        return $this->getById($id)->delete();
    }
}
