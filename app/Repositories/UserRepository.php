<?php


namespace App\Repositories;


use App\Http\Requests\UserRequest;
use App\Models\User;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class UserRepository implements IUserRepository
{
    public $model = null;
    protected $relations = null;

    public function __construct()
    {
        $this->model = new User();
        $this->relations = [];
    }


    public function baseQuery(): Builder
    {
        return $this->model::query()->with($this->relations);
    }

    public function getById(int $id): User
    {
        return $this->baseQuery()->find( $id);
    }

    public function store(UserRequest $request): User
    {
        $data = $request->only($this->model->getFillable());
        $data["password"] = "123";
        return $this->baseQuery()->create($data);
    }

    public function update(Request $request, int $id): int
    {
        return $this->getById($id)->update($request->only($this->model->getFillable()));
    }

    public function destroy(int $id)
    {
        return $this->getById($id)->delete();
    }
}
