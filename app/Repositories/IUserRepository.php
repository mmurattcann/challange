<?php


namespace App\Repositories;


use App\Http\Requests\UserRequest;
use App\Models\User;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;

interface IUserRepository
{
     public function baseQuery(): Builder;

    public function getById(int $id): User;

    public function store(UserRequest  $request): User;

    public function update(Request $request, int $id): int;

    public function destroy(int $id);

}
